package pl.rybak.reactivedemoserver.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.rybak.reactivedemoserver.domain.Game;

/**
 * ReactiveMongoRepository for Game Document
 */
public interface GamesReactiveRepository extends ReactiveMongoRepository<Game, ObjectId> {

}
