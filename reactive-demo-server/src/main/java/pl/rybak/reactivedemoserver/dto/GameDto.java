package pl.rybak.reactivedemoserver.dto;

import lombok.Data;


/**
 * Entity Dto
 */
@Data
public class GameDto {

    private String id;
    private String name;

}
