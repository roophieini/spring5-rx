package pl.rybak.reactivedemoserver.dto.mapper;

import org.bson.types.ObjectId;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.rybak.reactivedemoserver.domain.Game;
import pl.rybak.reactivedemoserver.dto.GameDto;

/**
 * Mapper interface for MapStruct.
 */
@Mapper(componentModel = "spring", imports = {ObjectId.class})
public interface GameMapper {

    //Simple mapping from Entity to Dto with custom expression
    @Mapping(target = "id", expression = "java( game.getId().toHexString() )")
    GameDto toDto(Game game);

    //Inherited configuration for entity from dto creation
    @InheritInverseConfiguration
    @Mapping(target = "id", expression = "java( new ObjectId(gameDto.getId()) )")
    Game toDocument(GameDto gameDto);

}
