package pl.rybak.reactivedemoserver.service.impl;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import pl.rybak.reactivedemoserver.dto.GameDto;
import pl.rybak.reactivedemoserver.dto.mapper.GameMapper;
import pl.rybak.reactivedemoserver.repository.GamesReactiveRepository;
import pl.rybak.reactivedemoserver.service.GameService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.Duration;

/**
 * Simple service with mappings
 */
@Service
@RequiredArgsConstructor
public class GameServiceImpl implements GameService {

    private final GamesReactiveRepository gameRepository;
    private final GameMapper gameMapper;

    @Override
    public Mono<GameDto> getById(ObjectId id) {
        return gameRepository
                .findById(id)
                .map(gameMapper::toDto);
    }

    @Override
    public Flux<GameDto> getAllGames() {
        return gameRepository
                .findAll()
                .map(gameMapper::toDto);
    }

    @Override
    public Mono<GameDto> save(Mono<GameDto> dto) {
        return gameRepository
                .insert(dto.map(gameMapper::toDocument))
                .next()
                .map(gameMapper::toDto);
    }

    @Override
    public Flux<GameDto> getAllWithWait() {
        return Flux.zip(Flux.interval(Duration.ofMillis(500)),
                gameRepository
                        .findAll()
                        .map(gameMapper::toDto))
                .map(Tuple2::getT2);
    }
}
