package pl.rybak.reactivedemoserver.service;

import org.bson.types.ObjectId;
import pl.rybak.reactivedemoserver.dto.GameDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface GameService {

    Mono<GameDto> getById(ObjectId id);

    Flux<GameDto> getAllGames();

    Mono<GameDto> save(Mono<GameDto> dto);

    Flux<GameDto> getAllWithWait();

}
