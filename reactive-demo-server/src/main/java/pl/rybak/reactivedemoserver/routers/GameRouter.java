package pl.rybak.reactivedemoserver.routers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import pl.rybak.reactivedemoserver.handler.GameHandler;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

/**
 * RouterFunction is a functional interface representation of
 * common rest controller.
 */
@Configuration
public class GameRouter {

    @Bean
    public RouterFunction<?> createRouter(GameHandler gameHandler) {
        return route(POST("/game"), gameHandler::handleSave)
                .and(route(GET("/games"), gameHandler::handleGetAll)
                        .and(route(GET("/game/{objectId}"), gameHandler::handleFindById)))
                            .and(route(GET("/games/wait"), gameHandler::handleGetWithWait));
    }
}