package pl.rybak.reactivedemoserver.domain;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Simple document
 */
@Document
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Game implements Serializable {


    private static final long serialVersionUID = 9134546252319057615L;

    @Id
    private ObjectId id;
    private String name;

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id.toString() +
                ", name='" + name + '\'' +
                '}';
    }
}
