package pl.rybak.reactivedemoserver.handler;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import pl.rybak.reactivedemoserver.dto.GameDto;
import pl.rybak.reactivedemoserver.service.GameService;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

/**
 * Handler for incoming http requests;
 */
@Component
@RequiredArgsConstructor
public class GameHandler {

    private final GameService gameService;

    /**
     * This handler method accepts {@link ServerRequest}
     * of and run service method.
     * After that, returns mono of {@link ServerResponse}
     * with saved GameDto object.
     * @param req - {@link ServerRequest}
     * @return
     */
    public Mono<ServerResponse> handleSave(ServerRequest req) {
        Mono<GameDto> mono = req.bodyToMono(GameDto.class);
        return ok().body(gameService.save(mono), GameDto.class);
    }

    /**
     * This handler invoike service method that
     * returns mono of {@link ServerResponse}
     * with {@link reactor.core.publisher.Flux} of GameDto object.
     * @return
     */
    public Mono<ServerResponse> handleGetAll(ServerRequest req) {
        return ok().body(gameService.getAllGames(), GameDto.class);
    }

    /**
     * This handler method accepts {@link ServerRequest} as a parameter
     * and return {@link Mono} of {@link ServerResponse}
     * with {@link Mono} of {@link GameDto} object.
     * @param req
     * @return
     */
    public Mono<ServerResponse> handleFindById(ServerRequest req) {
        ObjectId id = new ObjectId(req.pathVariable("objectId"));
        return ok().body(gameService.getById(id), GameDto.class);
    }

    /**
     * This handler invoike service method that
     * returns mono of {@link ServerResponse}
     * with {@link reactor.core.publisher.Flux} of GameDto object.
     * every 500 ms simulates processing big amount of data.
     * @return
     */
    public Mono<ServerResponse> handleGetWithWait(ServerRequest req) {
        return ok().contentType(TEXT_EVENT_STREAM).
                body(gameService.getAllWithWait(), GameDto.class);
    }
}
