package pl.rybak.reactivedemoserver.runner;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.bson.types.ObjectId;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.rybak.reactivedemoserver.domain.Game;
import pl.rybak.reactivedemoserver.repository.GamesReactiveRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Simple runner for db initialization
 */
@Component
@CommonsLog
@RequiredArgsConstructor
public class GameCLI implements CommandLineRunner {

    private static final String[] GAME_NAMES = {"WoW", "CoD", "CS:GO", "Tetris", "LoL", "Mario", "SWTOR", "Zelda",
            "The Witcher", "Doom", "Horizon Zero Down", "Dirt", "Guitar Hero"};
    private final GamesReactiveRepository gamesRepository;

    @Override
    public void run(String... args) {

        //Clear db
        gamesRepository.count()
                .flatMap(count -> count > 0 ? gamesRepository.deleteAll() : Mono.just(count));

        //Fluxes initialization
        Flux<ObjectId> idFlux = Flux.range(1, GAME_NAMES.length)
                .map(el -> ObjectId.get());
        Flux<String> nameFlux = Flux.just(GAME_NAMES);

        //Simple flux zip and save to db
        Flux<Game> gamesFlux = Flux.zip(idFlux, nameFlux, Game::new)
                .flatMap(gamesRepository::save);

        //Log to prove that games are saved
        gamesFlux.subscribe(null, null,
                () -> gamesRepository
                        .findAll()
                        .subscribe(mono -> log.info(mono.toString())));
    }
}
