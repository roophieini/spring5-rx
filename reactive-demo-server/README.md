# SpringBoot 2.0.0.RC2 Reactive Application with Netty, WebFlux and ReactiveMongo

#Usage:
 * Run server
 * execute `curl http://localhost:8080/games/` - this method returns all games at once
 * execute `curl http://localhost:8080/games/wait` - this method will return `text/event-stream`
 witch simulates how reactive stream push items when processing large amount of data
 
 